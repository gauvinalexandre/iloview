
# ILOView
**I**NTERACTIVE **L**IGHTWEIGHT **O**MNI **VIEW**ER

------------------------------------------------

## Installation instructions

For a simple local installation, download the source code. Then, in a terminal, go inside the root "iloview" folder and execute the following command:

`pip install .`

You can optionnaly use the PyQt5 (required on Mac OS) modules by using the following command:

`pip install .[qt]`

## Overview

This pure python viewer is meant to be used like matplotlib or a quick viewer and can:

- Vizualize different type of data together quickly and simply;
- Automatize figure making in high resolution;
- Make animations and videos;
- Help quality control;
- Better explore complex objects with cheap **3D glasses**;
- Vizualize your own custom objects;
- Build development prototypes, demos and presentations in Jupyter-Notebooks;
- ...

### What it is:

- Quick and easy
- Scriptable
- Pipelinable
- Callable from command line

### What it's not:

- It doesn't do everything like MI-Brain or TrackVis
- It's not a GUI

### Currently supported **data types** are:

- Volumetric scalar images
- Streamlines
- Masks or regions of interest (ROI)
- Labeled images

(Displayable in both voxel and world space)

### Coming soon...

- Meshes
- Tensors
- ODF
- voxel mm space support
- ...

## TODO:

- Completing the doc and refactoring the code for better maintenance and collaboration (plugins instead of huge class)
- Make a nice icon (any ideas?)
- Writing a citable abstract about ILOView
- Publish the code in a public repository (DIPY? SCILPY? Standalone?)
- Make a tutorial for video making
- Merge my other fixed XYZ slicing viewer into ILOView
- Develop new features (Many pending)

## Dependencies

- DIPY
- IPython
- Matplotlib
- Numpy
- PyQt5 (Optional, recommended for macOS)
- Scikit-Image
- Python VTK 6

You can find an quick tutorial in doc/
