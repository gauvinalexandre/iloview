from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()
long_description = (here / "README.md").read_text(encoding="utf-8")

setup(
    name="iloview",
    version="1.1",
    description="Interactive Lightweight Omni Viewer",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/gauvinalexandre/iloview",
    author="Alexandre Gauvin",
    author_email="gauvinalexandre@gmail.com",
    python_requires=">=3, <4",
    install_requires=[
        "IPython>=8.4",
        "dipy>=1.5",
        "fury>=0.8",
        "matplotlib>=3.5",
        "nibabel>=4",
        "numpy>=1.23",
        "pandas>=1.5",
        "scikit-image>=0.19",
        "vtk>=9.2",
    ],
    extras_require={
        "qt": [
            "PyQt5>=5",
        ],
    },
    entry_points={
        "console_scripts": [
            "iloview=iloview:main",
        ],
    },
)
