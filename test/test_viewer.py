import pytest

from iloview.viewer import ILOViewer
import nibabel as nib


@pytest.fixture
def fa_img():
    return nib.load("../doc/tutorial/data/fa.nii.gz")


def test_interpolation_nearest(fa_img):
    v = ILOViewer(fa_img.get_fdata(), affine=fa_img.affine, interp="nearest")
    v.show()


def test_interpolation_linear(fa_img):
    v = ILOViewer(fa_img.get_fdata(), affine=fa_img.affine, interp="linear")
    v.show()


def test_interpolation_cubic(fa_img):
    v = ILOViewer(fa_img.get_fdata(), affine=fa_img.affine, interp="cubic")
    v.show()
