from iloview.utils import compute_voxel_size
from iloview.utils import get_current_time_color
from iloview.utils import rotation_from_streamlines

from os import path
from sys import platform

from IPython.display import Image
from matplotlib import cm
import numpy as np
try:
    from PyQt5.QtWidgets import QApplication
    from iloview.qt_utils import QVTKRenderWindowInteractor
except:
    print("Warning: PyQt5 not installed, only VTK front end available.")
from skimage import measure
import vtk
vtk_ver = vtk.vtkVersion.GetVTKMajorVersion()
assert vtk_ver >= 6, "VTK {} detected, 6 required.".format(vtk_ver)
import vtk.util.numpy_support as np_support

from dipy.tracking.streamline import values_from_volume
from dipy.viz import colormap
from fury import actor

# TODO: Implement volume data color mapping and lookup tables (see viewer)
# TODO: Implement multiple volume blending (see vtkImageBlend from
#       Guillotine.py)
# TODO: Implement image slicer (XYZ cuts) in other option in addition to of
#       implicit plane slicer
# TODO: Add annotation for position and angle of view when ImageActor (from
#       viewer) is added (see vtkCornerAnnotation)
# TODO: Add docstrings
# TODO: Import everything from current viewer.py
# TODO: Add option of setting the image data
# TODO: Implement fodf visualisation (see fvtk.sphere_funcs)
# TODO: Script Subsample Streamlines de Francois for quick view of huge
# tractograms


class ILOViewer(object):
    key_actions = {
        "toggle-axis": {"key": "a", "desc": "Toggle axis visibility"},
        "quit": {"key": "q", "desc": "Quit"},
        "exit": {"key": "e", "desc": "Exit"},
        "fly": {"key": "f", "desc": "fly to the picked point"},
        "interact": {"key": "i", "desc": "Toggle interaction with plane widget"},
        "joystick": {"key": "j", "desc": "switch to joystick mode"},
        "snapshot": {"key": "o", "desc": "Save a snapshot in current directory"},
        "pick": {"key": "p", "desc": "Pick (select) object under pointer"},
        "planes": {"key": "s", "desc": "Show geometry as planes"},
        "trackball": {"key": "t", "desc": "switch to trackball mode"},
        "wires": {"key": "w", "desc": "Show geometry as wires"},
        "plane-sagittal": {"key": "x", "desc": "Set plane angle to sagittal"},
        "plane-coronal": {"key": "y", "desc": "Set plane angle to Coronal"},
        "plane-axial": {"key": "z", "desc": "Set plane angle to Axial"},
        "stereo": {"key": "3", "desc": "toggle stereo mode"},
        "rotate-down": {"key": "2", "desc": "Rotate scene to the down"},
        "rotate-left": {"key": "4", "desc": "Rotate scene to the left"},
        "rotate-right": {"key": "6", "desc": "Rotate scene to the right"},
        "rotate-counter-clockwise": {"key": "7", "desc": "Rotate scene counter clockwise"},
        "rotate-up": {"key": "8", "desc": "Rotate scene to the up"},
        "rotate-clockwise": {"key": "9", "desc": "Rotate scene clockwise"},
        "zoom-out": {"key": "-", "desc": "Zoom out"},
        "zoom-in": {"key": "+", "desc": "Zoom in"},
        "next": {"key": ">", "desc": "Select next slice"},
        "previous": {"key": "<", "desc": "Select previous slice"},
        "next-big": {"key": "Control->", "desc": "Select next slice (big step)"},
        "previous-big": {"key": "Control-<", "desc": "Select previous slice (big step)"},
        "camera-axial": {"key": "Shift-a", "desc": "Set camera (and plane) angle to Axial"},
        "camera-coronal": {"key": "Shift-c", "desc": "Set camera (and plane) angle to Coronal"},
        "camera-sagittal": {"key": "Shift-s", "desc": "Set camera (and plane) angle to sagittal"}}
    mouse_actions = {
        "camera-move": {"key": "Hold-Left-Click", "desc": "Move camera and widget"},
        "camera-zoom": {"key": "Hold-Right-Click", "desc": "Zoom camera"},
        "camera-zoom-step": {"key": "Scroll", "desc": "Zoom camera"},
        "camera-rotate": {"key": "Hold-Control-Left-Click", "desc": "Rotate Camera"},
        "camera-pan": {"key": "Hold-Shift-Left-Click", "desc": "Pan Camera"}}
    normal_cut = {
        "axial": (0, 0, 1),
        "coronal": (0, 1, 0),
        "sagittal": (1, 0, 0)}

    def help(self):
        print("Keyboard Actions")
        print("----------------")
        for k in sorted(self.key_actions.keys()):
            print("{} {}{}".format(
                self.key_actions[k]["key"],
                "." * (25 - len(self.key_actions[k]["key"])),
                self.key_actions[k]["desc"]))
        print("")

        print("Mouse Actions")
        print("-------------")
        for k in sorted(self.mouse_actions.keys()):
            print("{} {}{}".format(
                self.mouse_actions[k]["key"],
                "." * (25 - len(self.mouse_actions[k]["key"])),
                self.mouse_actions[k]["desc"]))

    def __init__(self, image=None, affine=None, clip=None, opacity=1.0, grayscale=True,
                 origin=False, bckgd=[0.18, 0.18, 0.18], display_info=True, interp="nearest"):
        """Interactive Lightweight Omni-Viewer (ILOView)"""

        self.renderer = vtk.vtkRenderer()
        self.renderer.SetBackground(*bckgd)
        self.text_actors = []
        self.plane_widget = None
        self.text = None

        light_kit = vtk.vtkLightKit()
        light_kit.AddLightsToRenderer(self.renderer)

        if affine is None:
            self.scale_unit = "vx"
        else:
            self.scale_unit = "mm"

        if origin:
            origin_axes = actor.axes(scale=(10, 10, 10))
            self.renderer.AddActor(origin_axes)

        if image is not None:
            self._init_slicer(image, affine, opacity, clip, grayscale, display_info, interp)

    def _init_slicer(self, image, affine=None, opacity=1.0, clip=None,
                     grayscale=True, display_info=True, interp="nearest"):
        """Initializes an arbitrary angle image slicer."""
        # Set info
        self.info = {}
        if display_info:
            self.text = vtk.vtkTextActor()
            self.text.SetTextScaleModeToProp()
            self.text.SetTextScaleModeToNone()
            self.text.SetDisplayPosition(10, 10)

            properties = self.text.GetTextProperty()
            properties.SetFontSize(12)
            properties.SetFontFamilyToCourier()
            properties.SetColor(
                *[1 - c for c in self.renderer.GetBackground()])

            self.renderer.AddActor(self.text)

        if len(image.squeeze().shape) == 3:
            nb_channels = 1
        else:
            nb_channels = 3
            image = image.astype(np.uint8)

        vtk_image = self._import_image(image, affine, interp)

        if nb_channels == 1:
            lookup_table = vtk.vtkLookupTable()
            lookup_table.SetNumberOfTableValues(1024)
            if clip is None:
                lookup_table.SetRange(image.min(), image.max())
            else:
                lookup_table.SetRange(*clip)
                lookup_table.UseBelowRangeColorOn()
                lookup_table.UseAboveRangeColorOn()
                lookup_table.SetBelowRangeColor([0.0, 0.0, 0.0, 0.0])
                lookup_table.SetAboveRangeColor([1.0, 1.0, 1.0, 0.0])
            if grayscale:
                lookup_table.SetValueRange(0.0, 1.0)
                lookup_table.SetSaturationRange(0.0, 0.0)
                lookup_table.SetHueRange(0.0, 0.0)
            lookup_table.Build()
            color_map = vtk.vtkImageMapToColors()
            color_map.SetLookupTable(lookup_table)
            color_map.PassAlphaToOutputOn()
            color_map.SetInputConnection(vtk_image)
            vtk_image = color_map.GetOutputPort()

        if affine is not None:
            if "voxel_size" not in self.__dict__: #FIXME: Why?
                spacing = self.voxel_size = compute_voxel_size(affine)
        else:
            spacing = [1.0, 1.0, 1.0]

        # Set slicing widget
        self.plane = vtk.vtkPlane()
        self.plane_widget = vtk.vtkImplicitPlaneWidget()
        self.plane_widget.SetInputConnection(vtk_image)
        self.plane_widget.SetDrawPlane(False)
        self.plane_widget.SetScaleEnabled(False)
        self.plane_widget.SetTubing(False)
        self.plane_widget.OutlineTranslationOff()
        self.plane_widget.OutsideBoundsOff()
        self.plane_widget.SetPlaceFactor(True)
        self.plane_widget.PlaceWidget()

        # Set slicer
        image_reslice_mapper = vtk.vtkImageResliceMapper()
        image_reslice_mapper.SetSlicePlane(self.plane)
        image_reslice_mapper.SetInputConnection(vtk_image)

        interpolator = vtk.vtkImageInterpolator()
        image_property = vtk.vtkImageProperty()
        if interp == "nearest":
            interpolator.SetInterpolationModeToNearest()
            image_reslice_mapper.ResampleToScreenPixelsOff()
            image_property.SetInterpolationTypeToNearest()
        elif interp == "linear":
            interpolator.SetInterpolationModeToLinear()
            image_reslice_mapper.ResampleToScreenPixelsOn()
            image_property.SetInterpolationTypeToLinear()
        elif interp == "cubic":
            interpolator.SetInterpolationModeToCubic()
            image_reslice_mapper.ResampleToScreenPixelsOn()
            image_property.SetInterpolationTypeToCubic()
        else:
            print("Not a valid interpolation option, using default.")

        image_reslice_mapper.SetInterpolator(interpolator)
        image_slice = vtk.vtkImageSlice()
        image_slice.SetProperty(image_property)
        image_slice.SetMapper(image_reslice_mapper)
        self.renderer.AddViewProp(image_slice)

        # Set axes
        self.axes = vtk.vtkCubeAxesActor()
        extent = self.plane_widget.GetInput().GetExtent()
        image_o = self.plane_widget.GetInput().GetOrigin()
        self.image_box = ((extent[0] * spacing[0] + image_o[0]),
                          (extent[1] * spacing[0] + image_o[0]),
                          (extent[2] * spacing[1] + image_o[1]),
                          (extent[3] * spacing[1] + image_o[1]),
                          (extent[4] * spacing[2] + image_o[2]),
                          (extent[5] * spacing[2] + image_o[2]))
        self.axes.SetBounds(*self.image_box)
        self.axes.SetTickLocationToBoth()
        self.axes.SetFlyModeToOuterEdges()
        self.axes.SetCamera(self.renderer.GetActiveCamera())
        self.axes.SetVisibility(False)
        self.axes.SetXUnits(self.scale_unit)
        self.axes.SetYUnits(self.scale_unit)
        self.axes.SetZUnits(self.scale_unit)
        self.renderer.AddActor(self.axes)

        extent = np.array(self.image_box).reshape((3, 2))
        self.middle = [np.mean(bound) for bound in extent]
        if self.plane_widget is not None:
            self.set_slicer(self.middle, "axial")
        self.set_camera_angle("axial")
        self.renderer.ResetCamera()
        self._update_info()

    def _init_interactor(self):
        """Initializes the viewer interactor."""
        inter_style = vtk.vtkInteractorStyleTrackballCamera()
        self.inter.SetInteractorStyle(inter_style)

        self.inter.AddObserver("KeyPressEvent", self._callback_viewer)

        if self.plane_widget is not None:
            self.inter.AddObserver("KeyPressEvent", self._callback_slicer)
            self.plane_widget.SetInteractor(self.inter)
            self.plane_widget.AddObserver(
                "InteractionEvent", self._callback_slicer)

        if len(self.text_actors) > 0:
            self.inter.AddObserver(
                "InteractionEvent", self._callback_annotations)

    def _callback_viewer(self, obj, event):
        """Callback function for viewer interaction."""
        key = obj.GetKeyCode()

        if key == "A":
            self.set_camera_angle("axial")
        elif key == "C":
            self.set_camera_angle("coronal")
        elif key == "S":
            self.set_camera_angle("sagittal")
        elif key == "R":
            self.set_camera_angle("axial")
            self.renderer.ResetCamera()
        elif key == "o":
            i = 0
            while path.isfile("snapshot_{}.png".format(i)):
                i += 1
            self.snapshot(snapshot_fn="snapshot_{}.png".format(i),
                          existing_window=obj.GetRenderWindow())
        elif key == "2":
            self.move_camera(elevation=15)
        elif key == "4":
            self.move_camera(azimuth=15)
        elif key == "6":
            self.move_camera(azimuth=-15)
        elif key == "7":
            self.move_camera(roll=15)
        elif key == "8":
            self.move_camera(elevation=-15)
        elif key == "9":
            self.move_camera(roll=-15)
        elif key == "-":
            self.move_camera(zoom=0.9)
        elif key == "+":
            self.move_camera(zoom=1.1)

        self.inter.Render()

    def _callback_annotations(self, obj, event):
        """Callback function for annotations camera follow."""
        if event == "InteractionEvent":
            for text_actor in self.text_actors:
                text_actor.SetCamera(self.renderer.GetActiveCamera())

    def _callback_slicer(self, obj, event):
        """Callback function for slicer interaction."""
        if event == "InteractionEvent":
            self.set_slicer()
        elif event == "KeyPressEvent":
            key = obj.GetKeyCode()
            control_pressed = obj.GetControlKey()

            if key == "A":
                self.set_slicer(normal="axial")
            elif key == "S":
                self.set_slicer(normal="sagittal")
            elif key == "C":
                self.set_slicer(normal="coronal")
            elif key == "R":
                self.set_slicer(origin=self.middle, normal="axial")
            elif key == "x":
                self.set_slicer(normal="sagittal")
            elif key == "y":
                self.set_slicer(normal="coronal")
            elif key == "z":
                self.set_slicer(normal="axial")
            elif key == "a":
                self.axes.SetVisibility(not self.axes.GetVisibility())
            elif key == "<" or key == ">":
                factor = 1.0
                if key == ">":
                    factor *= -1.0
                if control_pressed:
                    factor *= 5.0
                origin = self.plane_widget.GetOrigin()
                normal = self.plane_widget.GetNormal()
                self.set_slicer((origin[0] + factor * normal[0],
                                 origin[1] + factor * normal[1],
                                 origin[2] + factor * normal[2]))
        self._update_info()
        self.inter.Render()

    def _update_info(self):
        """Updates viewer info."""
        if self.text is not None:
            info_list = [
                "                    X       Y       Z",
                "Position:        {: 6.1f}  {: 6.1f}  {: 6.1f}".format(
                    *self.plane_widget.GetOrigin()),
                "Normal:          {: 6.2f}  {: 6.2f}  {: 6.2f}".format(
                    *self.plane_widget.GetNormal())]
            if self.scale_unit == "mm":
                info_list.append(
                    "Voxel size (mm): {: 6.3f}  {: 6.3f}  {: 6.3f}".format(
                        *self.voxel_size))

            info = "\n".join(info_list)

            self.text.SetInput(info)

    def _import_image(self, image, affine=None, interp="nearest"):
        """Imports and transforms a 3d numpy array to a vtk image."""
        if "smart_pointers" not in self.__dict__:
            self.smart_pointers = []

        image = image.squeeze()
        nb_dims = len(image.shape)
        if nb_dims == 3:
            [sx, sy, sz] = image.shape
            nb_channels = 1
            image = image.astype(np.float64)
        elif nb_dims == 4 and image.shape[3] == 3:
            [sx, sy, sz, nb_channels] = image.shape
        else:
            raise TypeError("Only 3D or 3D RGB data supported.")
        image = np.swapaxes(image, 0, 2)
        string_data = image.tostring()

        image_import = vtk.vtkImageImport()
        image_import.SetNumberOfScalarComponents(nb_channels)
        if nb_channels == 1:
            image_import.SetDataScalarTypeToDouble()
        else:
            image_import.SetDataScalarTypeToUnsignedChar()

        image_import.SetWholeExtent(0, sx - 1, 0, sy - 1, 0, sz - 1)
        image_import.SetDataExtentToWholeExtent()
        image_import.CopyImportVoidPointer(string_data, len(string_data))
        image_import.Update()
        vtk_image = image_import.GetOutputPort()
        self.smart_pointers.append(image_import)

        if affine is not None:
            spacing = compute_voxel_size(affine)

            image_reslice = vtk.vtkImageReslice()
            image_reslice.SetInputConnection(vtk_image)
            image_reslice.SetOutputDimensionality(3)
            transform = vtk.vtkTransform()
            transform.SetMatrix(affine.flatten())
            transform.Inverse()
            image_reslice.SetResliceTransform(transform)
            image_reslice.AutoCropOutputOn()

            if interp == "nearest":
                image_reslice.SetInterpolationModeToNearestNeighbor()
            elif interp == "linear":
                image_reslice.SetInterpolationModeToLinear()
            elif interp == "cubic":
                image_reslice.SetInterpolationModeToCubic()
            else:
                print("Not a valid interpolation option, using default.")

            image_reslice.SetOutputSpacing(*spacing)
            image_reslice.Update()  # FIXME: Why is this called?
            vtk_resliced_image = image_reslice.GetOutputPort()
            self.smart_pointers.append(image_reslice)
            return vtk_resliced_image

        return vtk_image

    def annotation(self, string, position, scale=1, color=(1, 1, 1),
                   pointer=None):
        """Adds annotation to the viewer."""
        text = vtk.vtkVectorText()
        text.SetText(string)
        text_mapper = vtk.vtkPolyDataMapper()
        text_mapper.SetInputConnection(text.GetOutputPort())
        text_actor = vtk.vtkFollower()
        text_actor.SetMapper(text_mapper)
        text_actor.SetScale(scale)
        text_actor.AddPosition(*position)
        text_actor.GetProperty().SetColor(*color)
        self.renderer.AddViewProp(text_actor)
        self.text_actors.append(text_actor)
        text_actor.SetCamera(self.renderer.GetActiveCamera())

        if pointer is not None:
            line = vtk.vtkLineSource()
            line.SetPoint1(*position)
            line.SetPoint2(*pointer)
            line.Update()
            line_mapper = vtk.vtkPolyDataMapper()
            line_mapper.SetInputConnection(line.GetOutputPort())
            line_actor = vtk.vtkActor()
            line_actor.SetMapper(line_mapper)
            line_actor.GetProperty().SetColor(*color)
            self.renderer.AddViewProp(line_actor)

        self.renderer.ResetCameraClippingRange()

    def streamlines(self, streamlines, color=None, color_affine=None,
                    color_name="Streamlines", lut=None, tube=False,
                    optiview=False, width=None, show_scalar_bar=True,
                    show_points=False):
        """Adds streamlines to the viewer."""
        if color is not None:
            cmap = np.array(color, dtype=np.float64)
            if cmap.ndim >= 3:
                # Make lookup table if not specified
                if lut is None:
                    if cmap.ndim == 3:
                        lut = colormap.colormap_lookup_table(
                            scale_range=(np.min(cmap), np.max(cmap)))
                    if cmap.ndim == 4 and cmap.shape[3] == 3:
                        cmap /= 255.0
                streamlines = [np.array(strml) for strml in streamlines]
                mapping = values_from_volume(cmap, streamlines, color_affine)
                pointwise_mapping = []
                for streamline_mapping in mapping:
                    pointwise_mapping += streamline_mapping
                cmap = np.array(pointwise_mapping)
            if lut is not None and show_scalar_bar:
                scalar_bar = actor.scalar_bar(lut, color_name)
                scalar_bar.SetPosition((0.05, 0.05))
                scalar_bar.SetWidth(0.1)
                scalar_bar.SetHeight(0.9)
                scalar_bar.SetBarRatio(0.2)
                scalar_bar.SetMaximumWidthInPixels(100)
                scalar_bar.SetNumberOfLabels(11)
                scalar_bar.SetLabelFormat("%-#8.1f")
                self.renderer.AddActor(scalar_bar)
        else:
            cmap = color

        if tube:
            if width is None:
                width = 0.1
            a = actor.streamtube(streamlines, colors=cmap, linewidth=width,
                                 lookup_colormap=lut)
            # TODO: Fix this
            # Turn off BFC because of a bug in VTK 6.3 affecting the labels of
            # the color bar
            # https://github.com/nipy/dipy/issues/1232
            a.GetProperty().BackfaceCullingOff()
        else:
            if width is None:
                width = 1
            a = actor.line(streamlines, colors=cmap, linewidth=width,
                           lookup_colormap=lut)
        self.renderer.AddActor(a)

        if show_points:
            if tube:
                for s in streamlines:
                    self.renderer.AddActor(
                        actor.point(s, colors=(1, 1, 1), opacity=1,
                              point_radius=2 * width))

            else:
                for s in streamlines:
                    self.renderer.AddActor(
                        actor.dot(s, colors=(1, 1, 1), opacity=1, dot_size=3 * width))

        if optiview:
            transfo = rotation_from_streamlines(streamlines)
            self.renderer.GetActiveCamera().ApplyTransform(transfo)

        if self.plane_widget is None:
            self.renderer.ResetCamera()
        self.renderer.ResetCameraClippingRange()

    def roi(self, roi, affine=None, level=None, opacity=1.0,
            color=(1.0, 0.0, 0.0), smooth=False, output_fn=None,
            interp="nearest"):
        """Adds the isosurface of an roi to the viewer."""
        roi = roi.astype(np.float64)
        if level is None:
            roi[roi > 0.0] = 1.0
        else:
            roi[roi != level] = 0.0
            roi[roi == level] = 1.0

        vtk_image = self._import_image(roi, affine, interp)

        contour = vtk.vtkMarchingCubes()
        contour.SetValue(0, 0.5)
        contour.SetInputConnection(vtk_image)

        contour.Update()
        surface_output = contour.GetOutputPort()

        if smooth:
            smoother = vtk.vtkSmoothPolyDataFilter()
            smoother.SetInputConnection(contour.GetOutputPort())
            smoother.SetRelaxationFactor(0.01)
#             smoother.BoundarySmoothingOff()
#             smoother.FeatureEdgeSmoothingOff()
            smoother.SetNumberOfIterations(256)
            smoother.Update()
            surface_output = smoother.GetOutputPort()

        normals = vtk.vtkPolyDataNormals()
        normals.SetInputConnection(surface_output)
        normals.SetFeatureAngle(60)

        surface_mapper = vtk.vtkPolyDataMapper()
        surface_mapper.SetInputConnection(normals.GetOutputPort())
        surface_mapper.ScalarVisibilityOff()

        surface = vtk.vtkActor()
        surface.SetMapper(surface_mapper)
        surface.GetProperty().SetOpacity(opacity)
        surface.GetProperty().SetColor(*color)

        self.renderer.AddActor(surface)
        if self.plane_widget is None:
            self.renderer.ResetCamera()
        self.renderer.ResetCameraClippingRange()

        if output_fn is not None:
            writer = vtk.vtkSTLWriter()
            writer.SetFileName(output_fn)
            writer.SetInputConnection(surface_output)
            writer.SetFileTypeToBinary()
            writer.Update()

    def labels(self, label_image, affine=None, labels=None, colors=None,
               opacity=1.0, annotations=True, smooth=False):
        """Adds isosurfaces of A labeled image to the viewer."""
        regions = measure.regionprops(label_image.astype(int), cache=True)
        props = {r.label: {
            "area": r.area,
            "bbox": r.bbox,
            "centroid": r.centroid}
            for r in regions if r.label in labels}

        if affine is not None:
            for p in props.values():
                def apply_3d_transform(coord, transform):
                    new_coord = np.ones(4)
                    new_coord[0:3] = coord
                    new_coord = np.dot(transform, new_coord)
                    return new_coord[0:3].tolist()
                p["bbox"] = apply_3d_transform(p["bbox"][:3], affine) + \
                    apply_3d_transform(p["bbox"][-3:], affine)
                p["centroid"] = apply_3d_transform(p["centroid"], affine)

        if labels is None:
            labels = [r.label for r in regions]
            for p in props.values():
                p["tag"] = "{} vx".format(p["area"])
        else:
            for p in props:
                props[p]["tag"] = labels[p] + \
                    " {} vx".format(props[p]["area"])

        if colors is None:
            r = np.linspace(0.0, 1.0, len(labels))
            colors = [c[0:3] for c in cm.gist_rainbow(r, bytes=False)]
            colors = {label: colors[i] for i, label in enumerate(labels)}

        for p in props:
            props[p]["color"] = colors[p]

        vtk_image = self._import_image(label_image, affine, interp="nearest")

        for p in props:
            contour = vtk.vtkDiscreteMarchingCubes()
            contour.SetValue(0, p)
            contour.SetInputConnection(vtk_image)
            contour.Update()
            surface_output = contour.GetOutputPort()

            if smooth:
                smoother = vtk.vtkSmoothPolyDataFilter()
                smoother.SetInputConnection(surface_output)
                smoother.SetRelaxationFactor(0.1)
                smoother.BoundarySmoothingOff()
                smoother.FeatureEdgeSmoothingOff()
                smoother.SetNumberOfIterations(128)
                smoother.Update()
                surface_output = smoother.GetOutputPort()

            normals = vtk.vtkPolyDataNormals()
            normals.SetInputConnection(surface_output)
            normals.SetFeatureAngle(60)

            surface_mapper = vtk.vtkPolyDataMapper()
            surface_mapper.SetInputConnection(normals.GetOutputPort())
            surface_mapper.ScalarVisibilityOff()

            surface = vtk.vtkActor()
            surface.SetMapper(surface_mapper)
            surface.GetProperty().SetOpacity(opacity)
            surface.GetProperty().SetColor(*props[p]["color"])

            self.renderer.AddActor(surface)

            if annotations:
                self.annotation(string=props[p]["tag"],
                                position=props[p]["bbox"][:3],
                                color=props[p]["color"],
                                scale=2,
                                pointer=props[p]["centroid"])

        if self.plane_widget is None:
            self.renderer.ResetCamera()
        self.renderer.ResetCameraClippingRange()

    def move_camera(self, azimuth=0, elevation=0, roll=0, zoom=0):
        """Moves the camera from its current position."""
        self.renderer.GetActiveCamera().Azimuth(azimuth)
        self.renderer.GetActiveCamera().Elevation(elevation)
        self.renderer.GetActiveCamera().Roll(roll)
        self.renderer.GetActiveCamera().Zoom(zoom)
        self.renderer.ResetCameraClippingRange()

    def set_camera_angle(self, angle):
        """Sets the camera to a particular angle."""
        cam = self.renderer.GetActiveCamera()
        position = np.array(cam.GetPosition())
        focus = np.array(cam.GetFocalPoint())
        distance = np.sqrt(np.sum((position - focus) ** 2))
        new_position = focus + distance * np.array(self.normal_cut[angle])
        cam.SetPosition(*new_position)
        cam.SetFocalPoint(*focus)
        if angle == "axial":
            cam.SetViewUp(0, 1, 0)
        else:
            cam.SetViewUp(0, 0, 1)
        self.renderer.ResetCameraClippingRange()

    def set_slicer(self, origin=None, normal=None):
        """Sets the slicer plane to a particular angle and position."""
        if origin is not None:
            self.plane_widget.SetOrigin(*origin)
        if normal is not None:
            if isinstance(normal, str):
                normal = self.normal_cut[normal]
            self.plane_widget.SetNormal(*normal)

        self.plane_widget.GetPlane(self.plane)

        rgb = np.abs(self.plane.GetNormal())
        self.plane_widget.GetNormalProperty().SetColor(*rgb)
        self.plane_widget.GetSelectedNormalProperty().SetColor(*rgb)
        self.renderer.ResetCameraClippingRange()

    def snapshot(self, snapshot_fn=None, render_size=(512, 512),
                 res_factor=1, rem_bckgd=False, existing_window=None,
                 get_array=False):
        """Renders a snapshot of the current scene."""
        if existing_window is None:
            self.renderer.SetUseDepthPeeling(True)
            self.renderer.SetOcclusionRatio(0.0)
            self.renderer.SetMaximumNumberOfPeels(0)
            window = vtk.vtkRenderWindow()
            window.AddRenderer(self.renderer)
            window.SetMultiSamples(0)
            window.SetAlphaBitPlanes(1)
            window.SetSize(*render_size)
            window.OffScreenRenderingOn()
            window.Render()
        else:
            window = existing_window

        printer = vtk.vtkWindowToImageFilter()
        printer.SetInput(window)
        printer.SetScale(res_factor)
        if rem_bckgd:
            printer.SetInputBufferTypeToRGBA()
        else:
            printer.SetInputBufferTypeToRGB()

        printer.Update()

        if get_array:
            image_data = printer.GetOutput()
            scalars = image_data.GetPointData().GetScalars()
            array = np_support.vtk_to_numpy(scalars)
            ws = window.GetSize()
            c = image_data.GetNumberOfScalarComponents()
            array = array.reshape((ws[1], ws[0], c))
            array = np.flipud(array)
            return array
        else:
            writer = vtk.vtkPNGWriter()
            if snapshot_fn is None:
                writer.WriteToMemoryOn()
            else:
                writer.SetFileName(snapshot_fn)
            writer.SetInputConnection(printer.GetOutputPort())
            writer.Write()

            if snapshot_fn is None:
                data = writer.GetResult()
                return Image(data)

    def show(self, window_size=(1024, 1024), title="ILOView", use_qt=None):
        """Displays the viewer in a window."""
        self.renderer.SetUseDepthPeeling(True)
        self.renderer.SetMaximumNumberOfPeels(10)
        self.renderer.SetOcclusionRatio(0.01)
        window = vtk.vtkRenderWindow()
        window.SetMultiSamples(0)
        window.SetAlphaBitPlanes(1)
        window.AddRenderer(self.renderer)

        if use_qt is None:
            if platform == "darwin":
                use_qt = True
            else:
                use_qt = False

        if use_qt:
            self.inter = vtk.vtkGenericRenderWindowInteractor()
            self._init_interactor()
            self.inter.SetRenderWindow(window)
            try:
                app = QApplication([title])
            except:
                print("Warning: PyQt5 not installed, rolling back to VTK GUI.")
                del self.inter
                self.show(window_size, title, "vtk")
                return
            widget = QVTKRenderWindowInteractor(rw=window, iren=self.inter)
            widget.resize(*window_size)
            widget.Initialize()
            widget.Start()
            widget.show()
            app.exec_()
        else:
            self.inter = vtk.vtkRenderWindowInteractor()
            self._init_interactor()
            window.SetSize(*window_size)
            window.SetWindowName(title)
            self.inter.SetRenderWindow(window)
            self.inter.Initialize()
            window.Render()
            self.inter.Start()

        del self.inter
    
    def ply(self, filename):
        print("Loading PLY file{}".format(filename))
        
        actor = vtk.vtkActor()
        self.renderer.AddActor(actor)
        reader = vtk.vtkPLYReader()
        reader.SetFileName(filename)
        norms = vtk.vtkTriangleMeshPointNormals()
        norms.SetInputConnection(reader.GetOutputPort())
        mapper = vtk.vtkOpenGLPolyDataMapper()
        mapper.SetInputConnection(norms.GetOutputPort())
        actor.SetMapper(mapper)
        actor.GetProperty().SetAmbientColor(0.2, 0.2, 1.0)
        actor.GetProperty().SetDiffuseColor(1.0, 0.65, 0.7)
        actor.GetProperty().SetSpecularColor(1.0, 1.0, 1.0)
        actor.GetProperty().SetSpecular(0.5)
        actor.GetProperty().SetDiffuse(0.7)
        actor.GetProperty().SetAmbient(0.5)
        actor.GetProperty().SetSpecularPower(20.0)
        actor.GetProperty().SetOpacity(1.0)

