import nibabel as nib
import nibabel.streamlines as nibs
from os import path, extsep

from iloview import viewer


desc = "The Interactive Lightweight Omni Viewer (ILOView)"

def set_parser(parser):
    parser.add_argument(
        "-v",
        dest="volume_filename",
        metavar="input_filename",
        help="Path of the scalar data volume to visualize")
    parser.add_argument(
        "-t",
        dest="tractogram_filenames",
        metavar="input_filename(s)",
        nargs="+",
        default=[],
        help="Path(s) of the tractograms to visualize")
    parser.add_argument(
        "--roi",
        dest="roi_filenames",
        metavar="input_filename(s)",
        nargs="+",
        default=[],
        help="Path(s) of the ROIs to visualize")
    parser.add_argument(
        "--cmap",
        dest="cmap_filename",
        metavar="input_filename",
        help="Colormap to use for tracts")
    parser.add_argument(
        "--origin",
        action="store_true",
        help="Show axes at origin.")
    parser.add_argument(
        "--tube",
        action="store_true",
        help="Show streamlines as tubes.")
    parser.add_argument(
        "--qt",
        action="store_true",
        help="Select Qt as a GUI backend (solves key catching bug in Mac OS).")

    parser.set_defaults(func=launch_iloview)


def launch_iloview(args):
    if args.volume_filename is None:
        iloview = viewer.ILOViewer(origin=args.origin)
    else:
        image = nib.load(args.volume_filename)
        iloview = viewer.ILOViewer(image.get_fdata(), image.affine, origin=args.origin)

    for tractogram_filename in args.tractogram_filenames:
        streamlines = nibs.load(tractogram_filename).streamlines
        if args.cmap_filename is None:
            iloview.streamlines(streamlines, tube=args.tube)
        else:
            cmap_image = nib.load(args.cmap_filename)
            cmap_name = path.basename(args.cmap_filename).split(extsep)[0]
            iloview.streamlines(streamlines, color=cmap_image.get_fdata(),
                                color_affine=cmap_image.affine,
                                color_name=cmap_name, tube=args.tube)

    for roi_filename in args.roi_filenames:
        roi = nib.load(roi_filename)
        iloview.roi(roi.get_fdata(), roi.affine, opacity=0.25)

    iloview.show(use_qt=args.qt)


def main():
    import argparse

    parser = argparse.ArgumentParser(description=desc)
    set_parser(parser)

    args = parser.parse_args()
    args.func(args)
