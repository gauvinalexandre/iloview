import numpy as np
from vtk import vtkMatrix4x4, vtkTransform

from dipy.core.ndindex import ndindex


def compute_voxel_size(affine):
    return [np.sqrt(np.sum(
        [np.square(affine[j, i]) for j in range(3)])) for i in range(3)]


def sinus_curve(x, y_min, y_max, dephasing):
    from numpy import sin, pi
    amp = (y_max - y_min) / 2.0
    offset = y_max - amp
    frequency = 1.0 / (x[-1] - x[0])
    phase = dephasing / frequency

    y = amp * sin(2.0 * pi * frequency * (x - phase)) + offset
    return y


def get_current_time_color():
    from time import struct_time, localtime
    from colorsys import hls_to_rgb
    from numpy import arange
    t = struct_time(localtime())
    current_time = t.tm_hour * 3600 + t.tm_min * 60 + t.tm_sec
    x = arange(0, 60 * 60 * 24)
    hue = sinus_curve(x, -0.33, 1.0 - 0.33, 0.25)
    lightness = sinus_curve(x, 0.18, 1.0 - 0.18, 0.25)
    saturation = sinus_curve(x, 0.36, 0.72, 0.25)

    def time_color(t): return hls_to_rgb(hue[t], lightness[t], saturation[t])

#     import matplotlib.pyplot as plt
#     plt.plot(x, lightness)
#     plt.plot(current_time, lightness[current_time], 'o', ms=16,
#              c=time_color(current_time))
#     x2 = arange(0, 60 * 60 * 24, 360)
#     for t in x2:
#         plt.plot(t, lightness[t], 'o', ms=4, c=time_color(t))
#     plt.show()

    return time_color(current_time)


def evec_from_streamlines(streamlines, use_line_dir=True):
    """ Get eigen vectors from lines directions in a 3x3 array

    if use_line_dir is set to False
        only use the points position information

    """

    if use_line_dir:
        lines_dir = []
        for line in streamlines:
            lines_dir += [line[1:] - line[0:-1]]
        directions = np.vstack(lines_dir)
    else:
        points = np.vstack(streamlines)
        centered_points = points - np.mean(points, axis=0)
        norm = np.sqrt(np.sum(centered_points**2, axis=1, keepdims=True))
        directions = centered_points / norm

    _, _, e_vec = np.linalg.svd(directions, full_matrices=False)
    return e_vec


def rotation_from_streamlines(streamlines, use_line_dir=True,
                              use_full_eig=True):
    """ Get the rotation from lines directions in vtkTransform() object

    if use_line_dir is set to False
        only use the points position information

    if use_full_eig is set to True
        the rotation will be the full eigen_vector matrix
        (not only the Yaw and Pitch)
    """
    e_vec = evec_from_streamlines(streamlines, use_line_dir)

    matrix = vtkMatrix4x4()

    if use_full_eig:
        for (i, j) in ndindex((3, 3)):
            matrix.SetElement(j, i, e_vec[i, j])

    else:
        v1 = e_vec[2]
        v2 = np.array([0, 0, 1])
        v3 = np.cross(v1, v2)
        v3 = v3 / (np.sqrt(np.sum(v3**2)))
        v4 = np.cross(v3, v1)
        v4 = v4 / (np.sqrt(np.sum(v4**2)))

        m1 = np.array([v1, v4, v3])
        cos = np.dot(v2, v1)
        sin = np.dot(v2, v4)
        m2 = np.array([[cos, sin, 0], [-sin, cos, 0], [0, 0, 1]])

        m = np.dot(np.dot(m1.T, m2), m1)

        for (i, j) in ndindex((3, 3)):
            matrix.SetElement(i, j, m[i, j])

    transform = vtkTransform()
    transform.SetMatrix(matrix)
    return transform
